/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package procesos;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
public class BorrarPalabras {
    
    private List<String> palabrasBorrar;
    private String nombreFinal;
    
    public BorrarPalabras() {
        palabrasBorrar = new LinkedList<>();
    }

    public String getNombreFinal() {
        return nombreFinal;
    }

    public void setNombreFinal(String nombreFinal) {
        this.nombreFinal = nombreFinal;
    }

    public List<String> getPalabrasBorrar() {
        return palabrasBorrar;
    }

    /**
     * Agrega una palabra a la lista lista que luego sera eliminada del nombre del archivo.
     * @param palabra Contiene la palabra que sera agregada a la lista.
     * @return Envia la palabra agregada.
     */
    public boolean addPalabrasBorrar(String palabra) {
        return palabrasBorrar.add(palabra);
    }

    /**
     * Método para borrar la ultima palabra que fue agregada a la lista.
     * @return Envia la palabra elimnada y si falla envia un dato nulo.
     */
    public String delUltimaPalabra(){
        if (palabrasBorrar.size() > 0) {
            return palabrasBorrar.remove(palabrasBorrar.size() -1);
        }
        return null;
    }
    
    /**
     * Método que sirve para borar la lista de las palabras por completo.
     * @return Envia verdadero si se borra correctamente, si falla envia un falso.
     */
    public boolean delTodasPalabras() {
        if (palabrasBorrar.size() > 0) return palabrasBorrar.removeAll(palabrasBorrar);
        return false;
    }
    
    
}
