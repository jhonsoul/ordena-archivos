/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package procesos;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JFileChooser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Jhon Alexander Buitrago Gonzalez
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RutasSistema {
    private String rutaBase;
    private String tipoArchivo;
    private List<File> subCarpetas;
    private List<File> urlSubArchivos;
    private List<File> listaArchivos;
    
    /**
     * Método usado para generar la ruta base con el que el sistema trabajara.
     * @return Entrega un mensaje de confirmación o no, si se creo la ruta.
     */
    public String setRutaBase() {
        JFileChooser ruta = new JFileChooser();
        ruta.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int acepto = ruta.showOpenDialog(null);
        if (acepto == 0) {
            rutaBase = ruta.getSelectedFile().getPath();
            return  "Se asigna " + rutaBase + " como la ruta base...";
        }            
        return "Se cancelo la asignación de la ruta base...";
    }
    
    /**
     * Genera la lista de sub-carpetas dentro de la ruta base.
     */
    public void setSubCarpetas() {
        if (rutaBase != null || !rutaBase.isEmpty()) {
            subCarpetas = new LinkedList<>();
            File[] carpetas = new File(rutaBase).listFiles();
            for (File carpeta : carpetas) {
                if (carpeta.isDirectory()) {
                    subCarpetas.add(carpeta);
                }
            }
        }
    }
    
    /**
     * Método que encuentra todos los archivos dentro de las sub-capetas y entrega al sistema una lista con los archivos.
     */
    public void setUrlSubArchivos(){
        if (!subCarpetas.isEmpty() || subCarpetas != null) {
            urlSubArchivos = new LinkedList<>();
            for (File subCarpeta : subCarpetas) {
                File[] archivosPC = new File(subCarpeta.toString()).listFiles();
                for (File file : archivosPC) {
                    if (file.isFile() && file.getName().endsWith(tipoArchivo)) {
                        urlSubArchivos.add(file);
                    }
                }
            }
        }
    }
    
    /**
     * Método para generar una lista de archivos que se encuentren dentro de la ruta base.
     */
    public void setListaArchivos() {
        if (rutaBase != null || !rutaBase.isEmpty()) {
            listaArchivos = new LinkedList<>();
            File[] archivos = new File(rutaBase).listFiles();
            for (File archivo : archivos) {
                if (archivo.isFile() && archivo.getName().endsWith(tipoArchivo)) {
                    listaArchivos.add(archivo);
                }
            }
        }
    }
    
    /**
     * Método para verificar si la ruta enviada contiene sub-carpetas.
     * @param ruta La ruta para hacer la verificacion.
     * @return Envia la confirmación o no de existencia de sub-carpetas.
     */
    public boolean contieneSubCarpetas(String ruta) {
        if (ruta != null) {
            File[] archivos = new File(ruta).listFiles();
            for (File archivo : archivos) {
                if (archivo.isDirectory()) {
                    return true;
                }
            }
        }
        return false;
    }
}
