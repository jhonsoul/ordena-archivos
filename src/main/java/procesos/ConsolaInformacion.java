/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package procesos;

import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import lombok.AllArgsConstructor;

/**
 * Se encarga de enviar la informacion al cuadro negro que regresenta una consola que entrega informacion al usuario final.
 * @author Jhon Alexander Buitrago Gonzalez
 */
@AllArgsConstructor
public class ConsolaInformacion {
    private final JTextArea consola;
    private final JProgressBar barraProgreso;
    
    /**
     * Método que hace posible la impresión de la información en la consola simulada y tambien aumenta el progreso en la barra.
     * @param log Contiene el nuevo texto que se imprimira en la consola.
     * @param progreso Es el valor numerico que se mostrara en la barra de progresión
     */
    public void mostrarMensaje(String log, int progreso){
        consola.append(log + "\n");
        barraProgreso.setValue(progreso);
    }
}
