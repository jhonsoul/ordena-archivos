/*
 * Copyright (C) 2021 Jhon Alexander Buitrago Gonzalez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package manipulacion_archivos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import lombok.Data;
import lombok.NoArgsConstructor;
import procesos.ConsolaInformacion;

/**
 * Clase para manejar todo las modificaciones que se realizaran en la aplicación.
 * @author Jhon Alexander Buitrago Gonzalez
 */

@Data
@NoArgsConstructor
public class ManejaArchivos {
    
    private ConsolaInformacion consola;
    private int porcentaje;
    private int cantidadPasos;
    private String extension;
    private String rutaBase;
    private List<File> archivos;

    public ManejaArchivos(ConsolaInformacion consola, String rutaBase) {
        this.consola = consola;
        this.rutaBase = rutaBase;
        cantidadPasos = 0;
    }
    
    /**
     * Método para contar la cantidad de métodos usados para el renombramiento y pueda ser calculado para la barra de progreso.
     * @param palabrasBorrar Contiene la lista con las palabras a borrar.
     * @param nuevoNombre Variable con el nombre que sera utilizado para modificar todos los archivos.
     */
    public void setCantidadPasos(List<String> palabrasBorrar, String nuevoNombre) {
        if (palabrasBorrar.size() > 0) cantidadPasos++;
        if (nuevoNombre != null) cantidadPasos++;
    }
    
    /**
     * Método para mover los archivos de subcarpetas a susubArchivoscarpeta principal.
     * @param subArchivos Contiene la lista de arvhico dentro de las carpetas dentro de la carpeta principal.
     */
    public void moverArchivos(List<File> subArchivos) {
        porcentaje = 0;
        if (subArchivos.size() > 0) {
            int puntoPorcentual = 30/subArchivos.size();
            subArchivos.forEach(subarchivo -> {
                try {
                    Path origen = Paths.get(subarchivo.toString());

                    //El destino debe tener el nombre del archivo como quedara.
                    Path destino = Paths.get(rutaBase + "\\" + origen.getFileName());

                    Files.move(origen, destino, StandardCopyOption.REPLACE_EXISTING);
                    porcentaje += puntoPorcentual;
                } catch (IOException ex) {
                    Logger.getLogger(ManejaArchivos.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        if (porcentaje < 29) porcentaje = 30;
    }
    
    /**
     * Realiza el renombrado de algunos archivos si se encuenta dentro de la lista entregada en el parametro.
     * @param palabrasBorrar Es una lista que contiene todas las palabras que sera eliminadas de los nombres de los archivos.
     */
    public void listaRenombrarArchivos(List<String> palabrasBorrar) {
        if (palabrasBorrar.size() > 0) {
            
            //Sesión para filtrar solo los archivos que contengan las palabras a borrar.
            List<File> lista = new LinkedList();
            for (File archivo : archivos) {
                if (archivo.isFile()) {
                    palabrasBorrar.forEach(palabraBorrar -> {
                        if (archivo.getName().contains(palabraBorrar)) {
                            lista.add(archivo);
                        }
                    });
                }
            }
            //Fin
            
            int puntoPorcentual = 70 / (cantidadPasos * lista.size());
            lista.forEach((t) -> {
                palabrasBorrar.forEach((palabraBorrar) -> {
                String[] nombreCortado = t.getName().split("\\.");
                nombreCortado[0] = nombreCortado[0].replace(palabraBorrar, "");
                File nuevoNombre = new File(rutaBase + "\\" + nombreCortado[0] + "." + extension);
                if (t.renameTo(nuevoNombre)) {
                    porcentaje += puntoPorcentual;
                    consola.mostrarMensaje("Se elimino " + palabraBorrar + " del archivo " + t.getName(), porcentaje);
                }
                });
            });
        }
    }
    
    /**
     * Método que se utiliza para renombrar todos los archivos.
     * @param nuevoNombre Contiene el nuevo nombre para los archivos de la carpeta.
     */
    public void renombrarArchivos(String nuevoNombre) {
        if (nuevoNombre != null) {
            int puntoPorcentual = 70 / (cantidadPasos * archivos.size());
            for (File archivo : archivos) {
                if (archivo.isFile()) {
                    String numeros = archivo.getName().split("\\.")[0].replaceAll("[^0-9]", "");
                    File archivoRenombrado = new File(rutaBase + "\\" + nuevoNombre + " - " + numeros + "." + extension);
                    if (archivo.renameTo(archivoRenombrado)) {
                        porcentaje += puntoPorcentual;
                        consola.mostrarMensaje("Se renombro a " + archivoRenombrado.getName() + "...", porcentaje);
                    }
                }
            }
        }
    }
    
    /**
     * Se encarga de eliminar el resto de archivos que no contenga la extension adecuada y tambien borra las carpetas.
     * @param extension Variable que sirve para validar los archivos y carpetas a borrar.
     */
    public void eliminarLoDemas(String extension) {
        int acepto = JOptionPane.showConfirmDialog(null, "Desea eliminar los demas archivos contenidos en la carpeta.");
        if (acepto == 0) {
            consola.mostrarMensaje("Se eliminaran los archivos distintos a lo selecionado...", 100);
            File[] archivosBorrar = new File(rutaBase).listFiles();
            for (File archivoBorrar : archivosBorrar) {
                if (!archivoBorrar.toString().endsWith(extension)) {
                    archivoBorrar.delete();
                }
            }
        }
        consola.mostrarMensaje("Proceso de movimiento y renombrado esta completo...", 100);
    }
}
